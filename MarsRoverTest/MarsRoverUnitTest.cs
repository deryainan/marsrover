﻿using MarsRover;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverTest
{
    [TestClass]
    public class MarsRoverUnitTest
    {
        [TestMethod]
        public void Test_MarsRover_Scenario1()
        {
            MarsRover.MarsRover marsRover = new MarsRover.MarsRover("5 5", "1 2 N", "LMLMLMLMM");
            string expected = marsRover.calculateCoordinates();
            Assert.AreEqual(expected, "1 3 N");
        }

        [TestMethod]
        public void Test_MarsRover_Scenario2()
        {
            MarsRover.MarsRover marsRover = new MarsRover.MarsRover("5 5", "3 3 E", "MMRMMRMRRM");
            string expected = marsRover.calculateCoordinates();
            Assert.AreEqual(expected, "5 1 E");
        }

        [TestMethod]
        public void Test_MarsRover_Scenario3()
        {
            MarsRover.MarsRover marsRover = new MarsRover.MarsRover("5 5", "5 4 N", "MMMMMMMMMMM");
            string expected = marsRover.calculateCoordinates();
            Assert.AreEqual(expected, "5 5 N");
        }

        [TestMethod]
        public void Test_MarsRover_Scenario4()
        {
            MarsRover.MarsRover marsRover = new MarsRover.MarsRover("6 6", "1 6 S", "MMMMMMMMMMM");
            string expected = marsRover.calculateCoordinates();
            Assert.AreEqual(expected, "1 0 S");
        }

        [TestMethod]
        public void Test_MarsRover_Scenario5()
        {
            MarsRover.MarsRover marsRover = new MarsRover.MarsRover("6 6", "1 6 W", "MMMMMMMMMMM");
            string expected = marsRover.calculateCoordinates();
            Assert.AreEqual(expected, "0 6 W");
        }

        [TestMethod]
        public void Test_MarsRover_Scenario6()
        {
            MarsRover.MarsRover marsRover = new MarsRover.MarsRover("6 6", "1 6 E", "MMMMMMMMMMM");
            string expected = marsRover.calculateCoordinates();
            Assert.AreEqual(expected, "6 6 E");
        }

        [TestMethod]
        public void Test_MarsRover_Scenario7()
        {
            MarsRover.MarsRover marsRover = new MarsRover.MarsRover("6 6", "3 3 N", "LLLLRRRR");
            string expected = marsRover.calculateCoordinates();
            Assert.AreEqual(expected, "3 3 N");
        }
    }
}
