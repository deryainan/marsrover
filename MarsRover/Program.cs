﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public class Program
    {
      
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter plateau upper-right coordinates(For Example: 5 5):");
            string plateuCoordinates = Console.ReadLine();
            Console.WriteLine("Please enter rover position(For Example: 1 2 N):");
            string roverCoordinates = Console.ReadLine();
            Console.WriteLine("Please enter instructions(For Example: LMLMLMLMM):");
            string nasaInstructions = Console.ReadLine();
            MarsRover marsRover = new MarsRover(plateuCoordinates, roverCoordinates, nasaInstructions);
            Console.WriteLine("Output:" + marsRover.calculateCoordinates());
            Console.ReadLine();
        }       

    }
}