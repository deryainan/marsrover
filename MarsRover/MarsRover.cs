﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public class MarsRover
    {
        public enum Position
        {
            M,
            L,
            R
        }
        public enum Direction
        {
            W,
            E,
            N,
            S
        }
        string plateuCoordinates = "";
        string roverCoordinates = "";
        string nasaInstructions = "";
        public MarsRover(string plateuCoordinates, string roverCoordinates, string nasaInstructions)
        {
            this.plateuCoordinates = plateuCoordinates;
            this.roverCoordinates = roverCoordinates;
            this.nasaInstructions = nasaInstructions;
        }
        public string calculateCoordinates()
        {            
            int plateuXMax = 0;
            int plateuYMax = 0;
            try
            {
                string[] plateuCoordinateList = plateuCoordinates.Split(' ');
                plateuXMax = int.Parse(plateuCoordinateList[0]);
                plateuYMax = int.Parse(plateuCoordinateList[1]);

                if (plateuCoordinateList.Length != 2)
                {
                    throw new Exception(plateuCoordinates + " is not valid.");
                }

                if (plateuXMax < 0)
                    throw new InvalidInputException(plateuXMax);
                if (plateuYMax < 0)
                    throw new InvalidInputException(plateuYMax);


                string[] roverCoordinateList = roverCoordinates.Split(' ');
                int roverX = int.Parse(roverCoordinateList[0]);
                int roverY = int.Parse(roverCoordinateList[1]);
                string roverDirection = roverCoordinateList[2];

                if (roverCoordinateList.Length != 3)
                {
                    throw new Exception(roverCoordinates + " is not valid.");
                }

                if (roverX < 0)
                    throw new InvalidInputException(roverX);
                if (roverY < 0)
                    throw new InvalidInputException(roverY);
                if (!Enum.IsDefined(typeof(Direction), roverDirection))
                    throw new Exception(roverDirection + " is not valid.");

                Direction direction = (Direction)Enum.Parse(typeof(Direction), roverDirection);

                char[] nasaInstructionsArr = nasaInstructions.ToCharArray();
                foreach (char item in nasaInstructionsArr)
                {
                    if (!Enum.IsDefined(typeof(Position), item.ToString()))
                    {
                        throw new Exception(item.ToString() + " instruction is not valid.");
                    }
                }

                Position[] nasaInstructionInput = nasaInstructions.Select(i => (Position)Enum.Parse(typeof(Position), i.ToString())).ToArray();

                foreach (Position position in nasaInstructionInput)
                {
                    if (direction == Direction.N)
                    {
                        if (position == Position.M)
                            roverY++;
                        else if (position == Position.L)
                            direction = Direction.W;
                        else if (position == Position.R)
                            direction = Direction.E;
                    }
                    else if (direction == Direction.S)
                    {
                        if (position == Position.M)
                            roverY--;
                        else if (position == Position.L)
                            direction = Direction.E;
                        else if (position == Position.R)
                            direction = Direction.W;
                    }
                    else if (direction == Direction.W)
                    {
                        if (position == Position.M)
                            roverX--;
                        else if (position == Position.L)
                            direction = Direction.S;
                        else if (position == Position.R)
                            direction = Direction.N;
                    }
                    else if (direction == Direction.E)
                    {
                        if (position == Position.M)
                            roverX++;
                        else if (position == Position.L)
                            direction = Direction.N;
                        else if (position == Position.R)
                            direction = Direction.S;
                    }
                }
                if (roverX > plateuXMax)
                    roverX = plateuXMax;
                else if (roverX < 0)
                    roverX = 0;

                if (roverY > plateuYMax)
                    roverY = plateuYMax;
                else if (roverY < 0)
                    roverY = 0;

                return roverX.ToString() + " " + roverY.ToString() + " " + roverDirection.ToString();            
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }            
        }
    }
}
