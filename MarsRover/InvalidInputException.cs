﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public class InvalidInputException : Exception
    {
        public InvalidInputException(int coordinate)
       : base(String.Format("Invalid coordinate : {0}", coordinate))
        {

        }
    }
}
